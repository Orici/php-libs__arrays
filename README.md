PHP - Arrays (Version: 0.0.01)
========================================================================
This library recopile several useful Array methods used in other
projects

    Moisés Alcocer
    2018, <contacto@ironwoods.es>
    https://www.ironwoods.es
========================================================================

    How to use
    ********************************************************************

        1. Create the directory "ironwoods" inside your's "vendors"
    directory
        2. Clone this library inside.
        3. Rename to "arrays" (if neccesary)
        4. Load the main file:
            require $vendors . '/ironwoods/arrays/arrays.php';
        Note: Alternatively to last step you can use a class in
    concrete, for example:
            require $vendors . '/ironwoods/arrays/classes/arraytools.php';
        5. Declare the class namespace:
            use ironwoods\libraries\arrays\Arrays as Arrays;
        or
            use ironwoods\libraries\arrays\classes\ArrayTools as ArrayTools;
        to use directly the class "ArrayTools".

        Warning: if you "require" the main facade class musn't "require"
    neither specific class


    Available methods (in the facade class)
    ********************************************************************

          - Arrays::cleanContent()      -> Cleans the content from array
          - Arrays::cleanRepeated()     -> Cleans the repeated elements
        from array
          - Arrays::cleanValues()       -> Cleans no value contents from
        array. Clean: nulls, zeros, empty arrays and empty strings
          - Arrays::replaceContent()    -> Replaces the strings inside
        the array


    Testing
    ********************************************************************

        Some unit tests, implemented with PHPUnit, are available in
    "tests/ArraysTests.php".
