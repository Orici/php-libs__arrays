<?php namespace ironwoods\libraries\Arrays\tests;
/*
cd c:/xampp/htdocs/desarrollos/libraries/_arrays/tests
phpunit ArraysTests.php --colors=always --repeat 10
*/

$_BASE_PATH = dirname( __FILE__, 2 ) . '/'; //Only PHP 7
require $_BASE_PATH . 'arrays.php';

use \PHPUnit\Framework\TestCase;
use ironwoods\libraries\arrays\Arrays as Arrays;


class ArraysTests extends TestCase
{

    /******************************************************************/
    /*** Properties declaration ***************************************/

        private $repeat_cities;
        private $repeat_names;
        private $repeat_numbers;
        private $shapes;


    /******************************************************************/
    /*** Methods declaration  *****************************************/

    /*** Public Methods ***********************************************/

        // Initialize the test case
        // Called for every defined test
        public function setUp()
        {
            $this->repeat_cities = [
                "London",
                "Madrid",
                "Madrid",
                "London",
                "Paris"
            ];
            $this->repeat_names = [
                "John",
                "Mark",
                "John",
            ];
            $this->repeat_numbers = [
                2,
                2,
                5,
                3,
                3,
                1
            ];
            $this->shapes = [
                'circle',
                'diamond',
                'square',
                'star',
                'triangle',
            ];
        }

        // Called once just like normal constructor
        // You can create database connections here etc
        public static function setUpBeforeClass()
        {
        }


    /**
     * Test methods
     *
     */

        /**
         * @coversDefaultClass \ironwoods\libraries\arrays\Arrays
         * @covers Arrays::cleanContent
         */
        public function testCleanContent(): void
        {
            $subject   = 'star';
            $expResult = [
                'circle',
                'diamond',
                'square',
                'triangle',
            ];

            $result = Arrays::cleanContent($this->shapes, $subject);
            self::assertEquals($expResult, $result);

            ////////////////////////////////////////////////////////////
            $subject   = 'London';
            $expResult = [
                "Madrid",
                "Madrid",
                "Paris"
            ];

            $result = Arrays::cleanContent(
                $this->repeat_cities,
                $subject
            );
            self::assertEquals($expResult, $result);

            ////////////////////////////////////////////////////////////
            $subject   = 2;
            $expResult = [
                5,
                3,
                3,
                1
            ];

            $result = Arrays::cleanContent(
                $this->repeat_numbers,
                $subject
            );
            self::assertEquals($expResult, $result);
        }

        /**
         * @coversDefaultClass \ironwoods\libraries\arrays\Arrays
         * @covers Arrays::cleanRepeated
         */
        public function testCleanRepeated(): void
        {
            $expResult = [
                "London",
                "Madrid",
                "Paris"
            ];

            $result = Arrays::cleanRepeated($this->repeat_cities);
            self::assertEquals($expResult, $result);

            ////////////////////////////////////////////////////////////
            $expResult = [
                "John",
                "Mark",
            ];

            $result = Arrays::cleanRepeated($this->repeat_names);
            self::assertEquals($expResult, $result);

            ////////////////////////////////////////////////////////////
            $expResult = [
                2,
                5,
                3,
                1
            ];

            $result = Arrays::cleanRepeated($this->repeat_numbers);
            self::assertEquals($expResult, $result);
        }

        /**
         * @coversDefaultClass \ironwoods\libraries\arrays\Arrays
         * @covers Arrays::cleanValues
         */
        public function testCleanValues(): void
        {
            $arr = [
                1,
                -5,
                '',
                'abc',
                null,
                0,
                [],
                [
                    null
                ]
            ];
            $expResult = [
                1,
                -5,
                'abc',
                [
                    null
                ]
            ];

            $result = Arrays::cleanValues($arr);
            self::assertEquals($expResult, $result);
        }

        /**
         * @coversDefaultClass \ironwoods\libraries\arrays\Arrays
         * @covers Arrays::replaceContent
         */
        public function testReplaceContent(): void
        {
            $replace   = 'xxx';
            $subject   = 'star';
            $expResult = [
                'circle',
                'diamond',
                'square',
                'xxx',
                'triangle',
            ];

            $result = Arrays::replaceContent(
                $this->shapes, $replace, $subject
            );
            self::assertEquals($expResult, $result);

            ////////////////////////////////////////////////////////////
            $subject   = 'London';
            $expResult = [
                "xxx",
                "Madrid",
                "Madrid",
                "xxx",
                "Paris"
            ];

            $result = Arrays::replaceContent(
                $this->repeat_cities, $replace, $subject
            );
            self::assertEquals($expResult, $result);
        }

} //class
