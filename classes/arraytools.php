<?php namespace ironwoods\libraries\arrays\classes;

/**
 * @file: arraytools.php
 * @info: class with methods to handle arrays
 *
 * @author: Moisés Alcocer
 * 2018, <contacto@ironwoods.es>
 * https://www.ironwoods.es
 *
 * @package ironwoods.libraries.arrays.classes
 * @version 0.0.1 (added)
 */

final class ArrayTools
{

    /******************************************************************/
    /*** Properties declaration ***************************************/


    /******************************************************************/
    /*** Methods declaration  *****************************************/


    /*** Public Methods ***********************************************/

        /**
         * Replaces the strings inside the array
         *
         * @param  array        $strs
         * @param  string       $replace
         * @param  string       $subject
         * @return array
         */
        public static function replaceContent(
            array $strs,
            string $replace,
            string $subject
        ): array
        {
            foreach ($strs as $key => $str) {

                if ($str === $subject) {
                    $str = str_replace($str, $replace, $subject);
                    $strs[ $key ] = $str;
                }
            }

            return $strs;
        }

} //class
