<?php namespace ironwoods\libraries\arrays;

/**
 * @file: arrays.php
 * @info: Main file for the librarie "Arrays"
 *
 *
 * @author: Moisés Alcocer
 * 2018, <contacto@ironwoods.es>
 * https://www.ironwoods.es
 *
 * @package ironwoods.libraries.arrays
 */

require 'classes/arrayfilters.php';
require 'classes/arraytools.php';

use ironwoods\libraries\arrays\classes\ArrayFilters as ArrayFilters;
use ironwoods\libraries\arrays\classes\ArrayTools as ArrayTools;


final class Arrays
{

    /******************************************************************/
    /*** Properties declaration ***************************************/


    /******************************************************************/
    /*** Methods declaration  *****************************************/

    /*** Public Methods ***********************************************/

        /**
         * Cleans the content from array
         *
         * @param  array        $arr
         * @param  mixed        $subject
         * @return array
         */
        public static function cleanContent(array $arr, $subject): array
        {
            return ArrayFilters::cleanContent($arr, $subject);
        }

        /**
         * Cleans the repeated elements from array
         *
         * @param  array        $arr
         * @return array
         */
        public static function cleanRepeated(array $arr): array
        {
            return ArrayFilters::cleanRepeated($arr);
        }

        /**
         * Cleans no value contents from array
         * Clean: nulls, zeros, empty arrays and empty strings
         *
         * @param  array        $arr
         * @return array
         */
        public static function cleanValues(array $arr): array
        {
            return ArrayFilters::cleanValues($arr);
        }

        /**
         * Replaces the strings inside the array
         *
         * @param  array        $strs
         * @param  string       $replace
         * @param  string       $subject
         * @return array
         */
        public static function replaceContent(
            array $strs,
            string $replace,
            string $subject
        ): array
        {
            return ArrayTools::replaceContent(
                $strs,
                $replace,
                $subject
            );
        }

} //class
