<?php namespace ironwoods\libraries\arrays\classes;

/**
 * @file: arrayfilters.php
 * @info: class with methods to filter array elements
 *
 * @author: Moisés Alcocer
 * 2018, <contacto@ironwoods.es>
 * https://www.ironwoods.es
 *
 * @package ironwoods.libraries.arrays.classes
 * @version 0.0.2 (added)
 */

final class ArrayFilters
{

    /******************************************************************/
    /*** Properties declaration ***************************************/


    /******************************************************************/
    /*** Methods declaration  *****************************************/


    /*** Public Methods ***********************************************/

        /**
         * Cleans the content from array
         *
         * @param  array        $arr
         * @param  mixed        $subject
         * @return array
         */
        public static function cleanContent(array $arr, $subject): array
        {
            foreach ($arr as $key => $value) {

                if ($value === $subject) {
                    unset($arr[ $key ]);
                }
            }

            // array_values() -> reindexe the array
            return array_values($arr);
        }

        /**
         * Cleans the repeated elements from array
         *
         * @param  array        $arr
         * @return array
         */
        public static function cleanRepeated(array $arr): array
        {
            foreach ($arr as $key => $value) {
                if (is_string($value)) {
                    $arr[ $key ] = trim($value);
                }
            }

            // array_unique() -> delete duplicate elements, no reindexes
            // array_values() -> reindexe the array
            return array_values(array_unique($arr));
        }

        /**
         * Cleans no value contents from array
         * Clean: nulls, zeros, empty arrays and empty strings
         *
         * @param  array        $arr
         * @return array
         */
        public static function cleanValues(array $arr): array
        {
            // array_filter() -> delete empty strings and nulls
            // array_values() -> reindexe the array
            return array_values(array_filter($arr));
        }

} //class
